export default class ClassroomWorkspace {
  public buildClassroomWorkspace (params: any) {
    const workspace = document.createElement('div')
    workspace.id = 'tableContainer'
    workspace.className = 'table-responsive'
    workspace.style.width = '100%'
    workspace.style.height = '100%'

    const table = document.createElement('table')
    table.className = 'table mb-0'
    table.id = 'tableMain'
    table.style.width = '100%'
    table.style.height = '100%'
    workspace.appendChild(table)

    // Foreach row in params.rows, create a tr element and append it to workspace
    // Foreach column in params.columns, create a td element and append it to the row
    for (let i = 0; i < params.rows; i++) {
      const row = document.createElement('tr')
      row.className = 'classroomRow'
      table.appendChild(row)

      let firstCell = document.createElement('td')
      for (let j = 0; j < params.columns; j++) {
        const column = document.createElement('td')
        column.className = 'classroomColumn'
        column.scope = 'col'
        column.id = `${i}${j}`
        row.appendChild(column)

        if (params.pos[i][j] === 1) {
        // Add a table
          const table = this.createTable(`table${i}${j}`)
          column.appendChild(table)
        } else if (params.pos[i][j] === 2) {
        // Add a whiteboard
        // If the previous or next column is a 2, fusion the two columns
          if (j > 0 && params.pos[i][j - 1] === 2) {
          // column.style.display = 'none'
            if (column.previousElementSibling) {
              firstCell.colSpan += 1
            }
            // Remove the column
            column.remove()
          } else {
            firstCell = column
            const teacher = document.createElement('div')
            teacher.className = 'teacherTable'
            teacher.innerHTML = 'Tableau'
            column.appendChild(teacher)
          }
        } else {
        // Add an empty cell
          const empty = document.createElement('div')
          empty.className = 'emptyTable'
          column.appendChild(empty)
        }
      }
    }
    return workspace
  }

  public createTable (id: string) {
    const table = document.createElement('div')
    table.className = 'studentTable'
    table.id = id
    return table
  }
}
