// This is a simple app to build a classroom plan
// User is able to design a classroom plan by adding tables (students and teachers). Theses tables can be added/deleted. User can also load a list of students and dispatch them on the tables (randomly or manually).

import ClassroomToolbar from './classroomToolbar'
import ClassroomWorkspace from './classroomWorkspace'
import '../css/style.css'
import html2canvas from 'html2canvas'

export class ClassroomPlan {
  private container: HTMLElement
  private params: any
  private workspace: HTMLElement
  private toolbar: HTMLElement
  private rows = 0
  private columns = 0
  private classroomToolbar: ClassroomToolbar
  private classroomWorkspace: ClassroomWorkspace

  constructor (container: HTMLElement) {
    this.classroomToolbar = new ClassroomToolbar()
    this.classroomWorkspace = new ClassroomWorkspace()
    this.container = container
    this.params = this.classroomToolbar.getClassroomDefaultParams()
    this.workspace = this.classroomWorkspace.buildClassroomWorkspace(this.params)
    this.toolbar = this.classroomToolbar.buildClassroomToolbar()
    this.rows = this.params.rows
    this.columns = this.params.columns
  }

  public init () {
    this.buildClassrom()
    this.addEventListeners()
  }

  private buildClassrom () {
    // Main container
    const content = document.createElement('div')
    content.style.position = 'relative'
    content.style.height = '100%'
    content.style.width = '100%'
    content.className = 'row m-0 h-100'
    content.id = 'classroomPlan'

    // Left container (workspace)
    const workspaceContainer = document.createElement('div')
    workspaceContainer.className = 'col-9 h-100'
    content.appendChild(workspaceContainer)

    // Right container (toolbar)
    const toolbarContainer = document.createElement('div')
    toolbarContainer.className = 'col-3'
    toolbarContainer.style.borderLeft = '1px solid var(--bs-light)'
    toolbarContainer.style.overflow = 'auto'
    content.appendChild(toolbarContainer)

    // Build the toolbar
    toolbarContainer.appendChild(this.toolbar)

    // Build the workspace
    workspaceContainer.appendChild(this.workspace)

    this.container.appendChild(content)
  }

  private addEventListeners () {
    // Event listener when modifying the number of rows
    const inputRows = document.getElementById('inputRows') as HTMLInputElement
    inputRows.addEventListener('change', (e) => {
      this.changeRow()
    })

    // Event listener when modifying the number of columns
    const inputColumns = document.getElementById('inputColumns') as HTMLInputElement
    inputColumns.addEventListener('change', (e) => {
      this.changeColumn()
    })

    //   Event listener when loading a list of students
    const inputLoad = document.getElementById('inputLoadStudents')
    inputLoad!.addEventListener('change', (e) => {
      this.loadStudents((e.target as HTMLInputElement).files![0])
    })

    // Event listener when placing students alphabetically
    const alphabeticallyButton = document.getElementById('orderButton')
    alphabeticallyButton!.addEventListener('click', () => {
      this.placeStudentsAlphabetically()
    })

    // Event listener when randomizing students
    const randomizeButton = document.getElementById('randomizeButton')
    randomizeButton!.addEventListener('click', () => {
      this.randomizeStudents()
    })

    // Event listener for student reset
    const putAllStudentsInListButton = document.getElementById('putAllStudentsInListButton')
    putAllStudentsInListButton!.addEventListener('click', () => {
      this.putAllStudentsInList(true)
    })

    // Event listener for fullscreen
    const fullscreenButton = document.getElementById('fullscreenClassroomButton')
    fullscreenButton!.addEventListener('click', () => {
      this.fullscreen()
    })

    // Event listener for picture capture
    const pictureButton = document.getElementById('pictureButton')
    pictureButton!.addEventListener('click', () => {
      this.captureWorkspacePicture()
    })

    // Bind
    this.dropToStudentList = this.dropToStudentList.bind(this)
    this.drag = this.drag.bind(this)
    this.drop = this.drop.bind(this)

    // For each .studentTable, add a drop event listener
    const studentTables = document.querySelectorAll('.studentTable')
    for (let i = 0; i < studentTables.length; i++) {
      const table = studentTables[i] as HTMLElement
      table.ondrop = this.drop
      table.ondragover = this.allowDrop
    }

    // Handle the #studentList drop event
    const studentsList = document.getElementById('studentsToolbarList')
    studentsList!.addEventListener('drop', this.dropToStudentList)
    studentsList!.ondragover = this.allowDrop

    // Handle #deleteClassroomTable click event
    const deleteClassroomTableButton = document.getElementById('deleteClassroomTableButton') as HTMLInputElement
    deleteClassroomTableButton!.onclick = () => {
      // Clear listeners
      this.clearListenersClassroomTools('deleteClassroomTableButton')
      // If is unchecked, return
      if (!deleteClassroomTableButton!.checked) {
        return
      }
      // Add a click event listener to .studentTable
      const studentTables = document.querySelectorAll('.studentTable')
      for (let i = 0; i < studentTables.length; i++) {
        const table = studentTables[i] as HTMLElement
        table.onclick = () => {
          this.deleteClassroomTable(table)
        }
      }
    }

    // Handle #addClassroomTable click event
    const addClassroomTableButton = document.getElementById('addClassroomTableButton') as HTMLInputElement
    addClassroomTableButton!.onclick = () => {
      // Clear listeners
      this.clearListenersClassroomTools('addClassroomTableButton')
      // If is unchecked, return
      if (!addClassroomTableButton!.checked) {
        return
      }
      // Add a click event listener to .classroomRow
      const classroomRows = document.querySelectorAll('.classroomColumn')
      for (let i = 0; i < classroomRows.length; i++) {
        const column = classroomRows[i] as HTMLElement
        column.onclick = () => {
          this.addClassroomTable(column)
        }
      }
    }
  }

  // Load a list of students from a CSV file
  private loadStudents (file: File) {
    this.classroomToolbar.loadCSV(file).then((students: unknown) => {
      // Hide the placeholder in the toolbar
      const placeholder = document.getElementById('studentsToolbarListPlaceholder')
      if (placeholder) {
        placeholder!.remove()
      }
      // Add the students to the list
      const studentsList = document.getElementById('studentsToolbarList')
      this.addStudentsToList(studentsList!, students as any[])
    })
  }

  // Randomize students on tables
  private randomizeStudents () {
    const studentTables = document.querySelectorAll('.studentTable')
    const studentsList = document.getElementById('studentsToolbarList')
    // Put all students in the list
    this.putAllStudentsInList()
    // Randomize the list
    const students = studentsList!.querySelectorAll('span')
    const studentsArray = Array.from(students)
    const shuffledStudents = studentsArray.sort(() => 0.5 - Math.random())
    // Add students to tables
    let i = 0
    for (let j = 0; j < studentTables.length; j++) {
      const studentTable = studentTables[j] as HTMLElement
      const student = shuffledStudents[i]
      if (student) {
        studentTable.appendChild(student)
        i++
      }
    }
    // Update the list order and counter
    this.orderList()
    this.updateCounter()
  }

  // Place students in alphabetical order
  private placeStudentsAlphabetically () {
    const studentTables = document.querySelectorAll('.studentTable')
    const studentsList = document.getElementById('studentsToolbarList')
    // Remove all students from the tables and put them in the list
    this.putAllStudentsInList()
    // Find all students in the list
    const students = studentsList!.querySelectorAll('span')
    const studentsArray = Array.from(students)
    // Order the list
    studentsArray.sort((a, b) => {
      if (a.innerHTML < b.innerHTML) {
        return -1
      }
      if (a.innerHTML > b.innerHTML) {
        return 1
      }
      return 0
    })
    // Place each element in the list in the tables
    let i = 0
    for (let j = 0; j < studentTables.length; j++) {
      const studentTable = studentTables[j] as HTMLElement
      const student = studentsArray[i]
      if (student) {
        studentTable.appendChild(student)
        i++
      }
    }
    // Update the list order and counter
    this.orderList()
    this.updateCounter()
  }

  // Put all students in the list
  private putAllStudentsInList (order = false) {
    const studentTables = document.querySelectorAll('.studentTable')
    const studentsList = document.getElementById('studentsToolbarList')
    // Remove all students from the tables and put them in the list
    for (let i = 0; i < studentTables.length; i++) {
      const studentTable = studentTables[i] as HTMLElement
      const students = studentTable.querySelectorAll('span')
      const studentsArray = Array.from(students)
      studentsArray.forEach(student => {
        studentsList!.appendChild(student)
      })
    }
    if (order) {
      // Update the list order and counter
      this.orderList()
      this.updateCounter()
    }
  }

  // Display the workspace in fullscreen
  private fullscreen () {
    const fullscreen = document.createElement('div')
    const classroomPlan = document.getElementById('classroomPlan')
    classroomPlan!.appendChild(fullscreen)
    fullscreen.style.position = 'fixed'
    fullscreen.style.top = '0'
    fullscreen.style.left = '0'
    fullscreen.style.width = '100%'
    fullscreen.style.height = '100%'
    fullscreen.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
    fullscreen.style.zIndex = '9999'
    fullscreen.style.display = 'flex'
    fullscreen.style.justifyContent = 'center'
    fullscreen.style.alignItems = 'center'
    fullscreen.style.fontSize = '12rem'
    fullscreen.style.color = 'white'
    fullscreen.style.userSelect = 'none'
    fullscreen.style.cursor = 'pointer'
    fullscreen.style.padding = '20px'
    fullscreen.innerHTML = document.getElementById('tableContainer')!.outerHTML

    fullscreen.addEventListener('click', () => {
      fullscreen.remove()
    })
  }

  // private zoomIn () {
  //   console.log('zoom in')
  //   this.params.display.zoom += 0.1
  //   const workspace = document.getElementById('tableContainer')
  //   workspace!.style.transform = `scale(${this.params.display.zoom})`
  // }

  // private zoomOut () {
  //   console.log('zoom out')
  //   this.params.display.zoom -= 0.1
  //   const workspace = document.getElementById('tableContainer')
  //   workspace!.style.transform = `scale(${this.params.display.zoom})`
  // }

  // Prevent the default handling
  private allowDrop (ev: DragEvent) {
    ev.preventDefault()
  }

  // Drag the element
  private drag (ev: DragEvent) {
    // Clear listeners to avoid conflicts
    this.clearListenersClassroomTools()
    // Get the id of the element being dragged
    const targetElement = ev.target as HTMLElement
    // Set the drag's data
    ev.dataTransfer!.setData('text/html', targetElement.id)

    this.updateCounter()
  }

  // Drop the element
  private drop (ev: DragEvent) {
    // Prevent the default handling
    ev.preventDefault()
    // Get the data
    const data = ev.dataTransfer!.getData('text/html')
    // Get the target element
    const targetElement = ev.target as HTMLElement
    // Get the element to append
    const elementToAppend = document.getElementById(data)
    // Handle swapping students between tables
    if (targetElement.tagName === 'SPAN') {
      // Swap the students
      const parentElement = targetElement.parentElement!
      const parentElementToAppend = elementToAppend!.parentElement!
      if (parentElementToAppend) {
        parentElement.appendChild(elementToAppend!)
      }
      if (parentElement) {
        parentElementToAppend.appendChild(targetElement)
      }
    } else {
      targetElement.appendChild(elementToAppend!)
    }

    this.updateCounter()
  }

  // Drop the element to the student list
  private dropToStudentList (ev: DragEvent) {
    // Prevent the default handling
    ev.preventDefault()
    // Get the data
    const data = ev.dataTransfer!.getData('text/html')
    // Get the target element
    const targetElement = document.getElementById('studentsToolbarList')
    // Get the element to append
    const elementToAppend = document.getElementById(data)
    if (elementToAppend) {
      targetElement!.appendChild(elementToAppend)
    }
    this.orderList()
    this.updateCounter()
  }

  // Add students to the list
  private async addStudentsToList (studentsList: HTMLElement, students: string[]) {
    return new Promise((resolve, reject) => {
      console.log('Adding students to list')
      console.log(students)
      studentsList!.innerHTML = ''
      ; (students as any[]).forEach((student: string) => {
        student = student.replace(/"/g, '')
        const span = document.createElement('span')
        span.id = 'student' + student
        span.draggable = true
        span.ondragstart = this.drag
        span.innerHTML = student
          studentsList!.appendChild(span)
      })
      const studentsNumber = document.getElementById('studentsNumber')
      if (studentsNumber) {
        studentsNumber.innerHTML = students.length.toString()
      }
    })
  }

  // Update the counter in the toolbar
  private updateCounter () {
    const studentsList = document.getElementById('studentsToolbarList')
    const students = studentsList!.querySelectorAll('span')
    const studentsArray = Array.from(students)
    const studentsNumber = document.getElementById('studentsNumber')
    if (studentsNumber) {
      studentsNumber.innerHTML = studentsArray.length.toString()
    }
  }

  // Sort the students in the list
  private orderList () {
    return new Promise<void>((resolve, reject) => {
      const studentsList = document.getElementById('studentsToolbarList')
      const students = studentsList!.querySelectorAll('span')
      const studentsArray = Array.from(students)
      studentsArray.sort((a, b) => {
        if (a.innerHTML < b.innerHTML) {
          return -1
        }
        if (a.innerHTML > b.innerHTML) {
          return 1
        }
        return 0
      })
      studentsList!.innerHTML = ''
      studentsArray.forEach(student => {
        studentsList!.appendChild(student)
      })
      resolve()
    }
    )
  }

  // Delete a table
  private deleteClassroomTable (table: HTMLElement) {
    // If a .studentTable is selected, remove it
    if (table) {
      // Check if the table is empty
      if (table.children.length > 0) {
        // If not empty, move the students to the list
        const studentsList = document.getElementById('studentsToolbarList')
        const students = table.querySelectorAll('span')
        students.forEach(student => {
          studentsList!.appendChild(student)
        })
      }
      // Add an emptyTable div
      const empty = document.createElement('div')
      empty.className = 'emptyTable'
      table.parentElement!.appendChild(empty)
      // Remove the table
      table.remove()
    }
  }

  // Add a table to the workspace
  private addClassroomTable (column: HTMLElement) {
    // Check if there is a div with the class .emptyTable
    const emptyTable = column.querySelector('.emptyTable')
    if (emptyTable) {
      // Remove the emptyTable div
      emptyTable.remove()
      // Add a new table to the workspace
      const table = document.createElement('div')
      table.className = 'studentTable'
      table.id = 'table' + column.id
      table.ondrop = this.drop
      table.ondragover = this.allowDrop
      // Add the table to the column
      column.appendChild(table)
    }
  }

  // Clear listeners on the classroom tools
  private clearListenersClassroomTools (element?: string) {
    // Clear listeners on .classroomRow
    const classroomRows = document.querySelectorAll('.classroomColumn')
    for (let i = 0; i < classroomRows.length; i++) {
      const column = classroomRows[i] as HTMLElement
      column.onclick = null
    }
    // Clear listeners on .studentTable
    const studentTables = document.querySelectorAll('.studentTable')
    for (let i = 0; i < studentTables.length; i++) {
      const table = studentTables[i] as HTMLElement
      table.onclick = null
    }

    // Uncheck the buttons
    const deleteTableButton = document.getElementById('deleteClassroomTableButton') as HTMLInputElement
    const addTableButton = document.getElementById('addClassroomTableButton') as HTMLInputElement
    if (element === 'addClassroomTableButton') {
      deleteTableButton.checked = false
    } else if (element === 'deleteClassroomTableButton') {
      addTableButton.checked = false
    } else {
      addTableButton.checked = false
      deleteTableButton.checked = false
    }
  }

  // Change the number of rows
  private changeRow () {
    // Clear listeners
    this.clearListenersClassroomTools()
    // Get the number of rows in the input
    const inputRows = document.getElementById('inputRows') as HTMLInputElement
    // Convert the value to a number
    const requestedRows = parseInt(inputRows.value)
    // Get the number of rows
    const rows = this.rows
    const table = document.getElementById('tableMain')
    if (requestedRows > rows) {
      const row = document.createElement('tr')
      row.className = 'classroomRow'
      table!.appendChild(row)

      for (let j = 0; j < 11; j++) {
        const column = document.createElement('td')
        column.className = 'classroomColumn'
        column.scope = 'col'
        column.id = `${rows}${j}`
        row.appendChild(column)

        // Add an empty cell
        const empty = document.createElement('div')
        empty.className = 'emptyTable'
        column.appendChild(empty)
      }
      this.rows += 1
    } else {
      // Find all .studentTable elements in the table!.lastElementChild! and put the students in the list
      const studentsList = document.getElementById('studentsToolbarList')
      const students = table!.lastElementChild!.querySelectorAll('span')
      const studentsArray = Array.from(students)
      studentsArray.forEach(student => {
        studentsList!.appendChild(student)
      })
      // Update the list order and counter
      this.orderList()
      this.updateCounter()
      // Remove the last row
      table!.lastElementChild!.remove()
      this.rows -= 1
    }
  }

  private changeColumn () {
    // Clear listeners
    this.clearListenersClassroomTools()
    // Get the number of columns in the input
    const inputColumns = document.getElementById('inputColumns') as HTMLInputElement
    // Convert the value to a number
    const requestedColumns = parseInt(inputColumns.value)
    // Get the number of columns in the last row
    const columns = this.columns
    const rows = document.getElementsByClassName('classroomRow')
    if (requestedColumns > columns) {
      for (let i = 0; i < rows.length; i++) {
        const row = rows[i] as HTMLElement
        const column = document.createElement('td')
        column.className = 'classroomColumn'
        column.scope = 'col'
        column.id = `${i}${columns + 1}`
        row.appendChild(column)

        // Add an empty cell
        const empty = document.createElement('div')
        empty.className = 'emptyTable'
        column.appendChild(empty)
      }
      this.columns += 1
    } else {
      // Find all .studentTable elements in the last column and put the students of the last column in the list
      const studentsList = document.getElementById('studentsToolbarList')
      for (let i = 0; i < rows.length; i++) {
        const row = rows[i] as HTMLElement
        const column = row.lastElementChild!
        const students = column.querySelectorAll('span')
        const studentsArray = Array.from(students)
        studentsArray.forEach(student => {
          studentsList!.appendChild(student)
        })
        column.remove()
      }
      // Update the list order and counter
      this.orderList()
      this.updateCounter()
      this.columns -= 1
    }
  }

  // Capture the workspace as a picture
  private captureWorkspacePicture () {
    // Capture the workspace as a picture and download it
    const workspace = document.getElementById('tableMain')! as HTMLCanvasElement
    html2canvas(workspace).then((canvas: HTMLCanvasElement) => {
      const link = document.createElement('a')
      link.download = 'classroomPlan.png'
      link.href = canvas.toDataURL('image/png')
      link.click()
    })
  }
}
