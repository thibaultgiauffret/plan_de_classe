/*
* main.ts
* This file is the main entry point for the application.
*/
import { Popover } from 'bootstrap'
import '@fortawesome/fontawesome-free/css/all.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { ClassroomPlan } from './ts/main'

const classroomPlan = new ClassroomPlan(document.getElementById('classroomApp')!)
classroomPlan.init()

// Init all popovers
const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]');
[...popoverTriggerList].map((popoverTriggerEl) => new Popover(popoverTriggerEl))