# Plan de classe

"Plan de classe" est une application web permettant - comme son nom l'indique - de générer des plans de classe (à partir d'une liste d'élèves exportée depuis Pronote par exemple).

Cette application a été développée dans l'optique d'une intégration à l'outil "Tableau" d'EnSciences ([tableau.ensciences.fr](https://tableau.ensciences.fr)) dont les sources sont disponibles [juste ici](https://framagit.org/ThibGiauffret/tableau).

Une version en ligne de "Plan de classe" est accessible [ici](https://www.ensciences.fr/addons/plan_de_classe/) ou [là](https://thibaultgiauffret.forge.apps.education.fr/plan_de_classe/).

**🚧 ATTENTION :** Ce projet est en cours de développement. Il n'est pas encore prêt pour une utilisation en production.


## 🖥 Tester en local

- Installer NodeJS et NPM
- Installer git : [https://git-scm.com/downloads](https://git-scm.com/downloads)
- Cloner le dépôt : `git clone https://forge.apps.education.fr/thibaultgiauffret/plan_de_classe.git`
- Installer les dépendances : `npm install`
- Lancer le serveur local de développement : `npm run test`

## 🛠 Contribuer

Vous pouvez contribuer au projet en proposant des améliorations ou des corrections de bugs [juste là](https://forge.apps.education.fr/thibaultgiauffret/plan_de_classe/-/issues) ou en proposant des requêtes de fusion [ici](https://forge.apps.education.fr/thibaultgiauffret/plan_de_classe/-/merge_requests).

## ⚖ Licence

"Plan de classe" est sous licence GNU GPL v3. Vous pouvez consulter le texte complet de la licence [ici](https://www.gnu.org/licenses/gpl-3.0.html).